SELECT q.id_nic
        , q.nic
        , TO_CHAR(q.fecha_nic,'DD/MM/YYYY') fecha_nic
        , q.hora_nic
        , q.nuc
        , TO_CHAR(q.fecha_hora_registro,'DD/MM/YYYY') fecha_registro
        , TO_CHAR(q.fecha_hora_registro,'HH24:MI:SS') hora_registro
        , q.fiscalia
        , (select lif1.desc_item from sigi_ldap_item_fix lif1 where lif1.tipo_item='FISCALIA' and lif1.codigo_item=q.fiscalia) nombre_fiscalia
        , q.agencia
        , (select lif2.desc_item from sigi_ldap_item_fix lif2 where lif2.tipo_item='AGENCIA' and lif2.codigo_item=q.agencia) nombre_agencia
        , (SELECT dnic.descripcion
             FROM nic dnic
            WHERE dnic.id=q.id_nic
          ) descripcion
        , (SELECT pre1.hechos_narrados
             FROM predenuncia pre1
            WHERE pre1.caso_id=q.id_nic
              AND dbms_lob.getlength(pre1.hechos_narrados)=(SELECT MAX(dbms_lob.getlength(pre2.hechos_narrados))
                                                              FROM predenuncia pre2
                                                             WHERE pre2.caso_id=q.id_nic
              AND rownum=1)
          ) narracion_extensa
        , (SELECT ent1.narracion_hechos
             FROM entrevista ent1
            WHERE ent1.caso_id=q.id_nic
              AND ent1.id=(SELECT MIN(ent2.id)
                             FROM entrevista ent2
                            WHERE ent2.caso_id=q.id_nic)
          ) entrevista
        , q.tipo_lugar
        , q.estado
        , q.municipio
        , q.localidad
        , q.colonia
        , q.calle
        , q.id_delito
        , q.delito
        , q.modalidad_delito
        , q.forma_accion
        , q.forma_comision
        , (SELECT SUBSTR(SYS_CONNECT_BY_PATH(tipa2.tipo_arma, ','),2) tipo_armas
             FROM (SELECT tipa1.tipo_arma
                        , tipa1.id_nic
                        , COUNT(*) OVER (PARTITION BY tipa1.id_nic) cnt
                        , ROW_NUMBER () OVER (PARTITION BY tipa1.id_nic ORDER BY tipa1.tipo_arma) seq
                     FROM (SELECT DISTINCT ta2.nombre tipo_arma
                                         , ar2.nic_id id_nic
                             FROM arma ar2
                                , tipo_arma ta2
                            WHERE ar2.nic_id=q.id_nic
                              AND ta2.id=ar2.tipo_arma_id) tipa1) tipa2
                    WHERE tipa2.seq=cnt
                   START WITH tipa2.seq=1
                   CONNECT BY PRIOR tipa2.seq+1=tipa2.seq
                          AND PRIOR tipa2.id_nic=tipa2.id_nic
          ) tipo_armas
        , TO_CHAR(CAST(q.fecha_hora_hechos AS DATE),'DD/MM/YYYY') fecha_hechos
        , TO_CHAR(CAST(q.fecha_hora_hechos AS DATE),'HH24:MI:SS') hora_hechos
        , q.tipo_zona
        , q.tipo_interviniente
        , q.nombre
        , q.paterno
        , q.materno
        , q.sexo
        , q.edad
        , DECODE(q.detenido,1,'DETENIDO',NULL) detenido
        , q.user_name_asignado
        , (select lif3.desc_item from sigi_ldap_item_fix lif3 where lif3.tipo_item='TITULAR' and lif3.codigo_item=q.user_name_asignado) nombre_titular
        , q.no_folio_constancia
        , q.clasificaciones
        , dbms_lob.substr( abi.descripcion, 2500, 1 ) BIENES_ASEGURADOS
     FROM (SELECT DISTINCT x.id_nic
                         , x.nic
                         , x.fecha_nic
                         , x.hora_nic
                         , x.nuc
                         , x.fecha_hora_registro
                         , x.fiscalia
                         , x.agencia
                         , x.tipo_lugar
                         , x.estado
                         , x.municipio
                         , x.localidad
                         , x.colonia
                         , x.calle
                         , x.id_delito
                         , x.delito
                         , x.modalidad_delito
                         , x.forma_accion
                         , x.forma_comision
                         , x.fecha_hora_hechos
                         , x.tipo_zona
                         , x.tipo_interviniente
                         , x.nombre
                         , x.paterno
                         , x.materno
                         , x.sexo
                         , x.edad
                         , x.detenido
                         , x.user_name_asignado
                         , x.no_folio_constancia
                         , x.clasificaciones
                         
             FROM v_consultas_municipio x

            WHERE 
            x.fecha_hora_registro BETWEEN 
            TO_DATE('$pFechaBeg  000000','DDMMYYYY HH24MISS') 
                AND TO_DATE('$pFechaEnd 195959','DDMMYYYY HH24MISS')
            AND 
            x.ID_DELITO IN (
                            200,			
                            186,	       
                            918, 			
                            202)			

          ) q
          
        LEFT JOIN NIC_ASEGURAMIENTO_BIENES nba ON nba.NIC_ID = q.id_nic
        LEFT JOIN ASEGURAMIENTO_BIENES abi ON abi.id = nba.ASEGURAMIENTO_BIENES_ID
                
    ORDER BY DECODE(q.nuc,NULL,1,0)
           , q.nic
           , q.nuc
           , q.tipo_interviniente
           , q.nombre
           , q.paterno
           , q.materno
           , q.tipo_lugar
           , q.modalidad_delito
           , q.forma_accion
           , q.forma_comision






